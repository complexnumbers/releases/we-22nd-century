# Build files for "We, 22nd Century" album

Index file: https://complexnumbers.gitlab.io/releases/we-22nd-century/

## License
### We, 22nd Century (c) by Victor Argonov Project

We, 22nd Century is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
